// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.scss';
import NxWelcome from './nx-welcome';

import { Route, Link } from 'react-router-dom';

import { LibraryReactExport2 } from '@merqueo/library-react-export-2';

export function App() {
  return (
    <>
      <h1>Welcome to Merqueo Library with NX.dev!</h1>
      <LibraryReactExport2 />
    </>
  );
}

export default App;
