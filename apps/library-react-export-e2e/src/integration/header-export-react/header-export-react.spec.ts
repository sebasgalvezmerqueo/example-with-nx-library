describe('library-react-export: HeaderExportReact component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=headerexportreact--primary'));
    
    it('should render the component', () => {
      cy.get('h1').should('contain', 'Welcome to HeaderExportReact!');
    });
});
