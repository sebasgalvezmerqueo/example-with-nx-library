describe('library-react-export: ButtonExportReact component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=buttonexportreact--primary&args=titleBtn;'));
    
    it('should render the component', () => {
      cy.get('h1').should('contain', 'Welcome to ButtonExportReact!');
    });
});
