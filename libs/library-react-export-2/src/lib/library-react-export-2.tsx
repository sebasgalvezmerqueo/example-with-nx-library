import styles from './library-react-export-2.module.scss';
import { ButtonExportReact } from './button-export-react/button-export-react';
import { HeaderExportReact } from './header-export-react/header-export-react';

/* eslint-disable-next-line */
export interface LibraryReactExport2Props {}

export function LibraryReactExport2(props: LibraryReactExport2Props) {
  const titleBtn = 'Press Me (Button)';
  return (
    <div className={styles['container']}>
      <h1>This is Header!</h1>
      <HeaderExportReact />
      <h1>Welcome to Button!</h1>
      <ButtonExportReact titleBtn={titleBtn} />
    </div>
  );
}

export default LibraryReactExport2;
