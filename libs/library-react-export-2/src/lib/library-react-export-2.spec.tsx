import { render } from '@testing-library/react';

import LibraryReactExport2 from './library-react-export-2';

describe('LibraryReactExport2', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LibraryReactExport2 />);
    expect(baseElement).toBeTruthy();
  });
});
