import { render } from '@testing-library/react';

import HeaderExportReact from './header-export-react';

describe('HeaderExportReact', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<HeaderExportReact />);
    expect(baseElement).toBeTruthy();
  });
});
