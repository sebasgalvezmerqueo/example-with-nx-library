import styles from './button-export-react.module.scss';

/* eslint-disable-next-line */
export interface ButtonExportReactProps {
  titleBtn: string;
}

export function ButtonExportReact(props: ButtonExportReactProps) {
  const title = props.titleBtn;
  const functionTest = () => {
    console.log('click button');
  };
  return (
    <div className={styles['container']}>
      <button onClick={functionTest}>{title}</button>
    </div>
  );
}

export default ButtonExportReact;
