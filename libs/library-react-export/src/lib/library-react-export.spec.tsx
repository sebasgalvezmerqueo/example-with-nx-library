import { render } from '@testing-library/react';

import LibraryReactExport from './library-react-export';

describe('LibraryReactExport', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LibraryReactExport />);
    expect(baseElement).toBeTruthy();
  });
});
