import styles from './library-react-export.module.scss';
import { ButtonExportReact } from './button-export-react/button-export-react';
import { HeaderExportReact } from './header-export-react/header-export-react';

/* eslint-disable-next-line */
export interface LibraryReactExportProps {}

export function LibraryReactExport(props: LibraryReactExportProps) {
  const titleBtn = 'Press Me (Button)';
  return (
    <div className={styles['container']}>
      <h1>Welcome to Merqueo Library!</h1>
      <h1>This is Header!</h1>
      <HeaderExportReact />
      <h1>Welcome to Button!</h1>
      <ButtonExportReact titleBtn={titleBtn} />
    </div>
  );
}

export default LibraryReactExport;
