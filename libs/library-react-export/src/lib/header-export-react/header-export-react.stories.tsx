import { Story, Meta } from '@storybook/react';
import {
  HeaderExportReact,
  HeaderExportReactProps,
} from './header-export-react';

export default {
  component: HeaderExportReact,
  title: 'HeaderExportReact',
} as Meta;

const Template: Story<HeaderExportReactProps> = (args) => (
  <HeaderExportReact {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
