import { Story, Meta } from '@storybook/react';
import {
  ButtonExportReact,
  ButtonExportReactProps,
} from './button-export-react';

export default {
  component: ButtonExportReact,
  title: 'ButtonExportReact',
} as Meta;

const Template: Story<ButtonExportReactProps> = (args) => (
  <ButtonExportReact {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  titleBtn: '',
};
