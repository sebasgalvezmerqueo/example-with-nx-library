import { render } from '@testing-library/react';

import ButtonExportReact from './button-export-react';

describe('ButtonExportReact', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ButtonExportReact />);
    expect(baseElement).toBeTruthy();
  });
});
