import { Story, Meta } from '@storybook/react';
import {
  LibraryReactExport,
  LibraryReactExportProps,
} from './library-react-export';

export default {
  component: LibraryReactExport,
  title: 'LibraryReactExport',
} as Meta;

const Template: Story<LibraryReactExportProps> = (args) => (
  <LibraryReactExport {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
